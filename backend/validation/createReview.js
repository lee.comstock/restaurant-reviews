const validator = require("validator");
const isEmpty = require("is-empty");
const moment = require("moment");

module.exports = function validateCreateReviewInput(data) {
  let errors = {};
  // convert empty fields to an empty string so we can use validator functions
  data.comment = !isEmpty(data.comment) ? data.comment.trim() : "";
  data.date = !isEmpty(data.date) ? data.date.trim() : "";
  data.rating = !isEmpty(data.rating) ? data.rating : "";
  // comment checks
  if (validator.isEmpty(data.comment)) {
    errors.comment = "Comment field is required";
  }
  // date checks
  if (validator.isEmpty(data.date)) {
    errors.date = "Date field is required";
  } else if (!moment(data.date, ["MM-DD-YYYY", "YYYY-MM-DD"]).isValid()) {
    errors.date = "Date is invalid";
  }
  // rating checks
  if ([1,2,3,4,5].indexOf(data.rating) == -1) {
    errors.rating = "Rating needs to be a number from 1 to 5";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
