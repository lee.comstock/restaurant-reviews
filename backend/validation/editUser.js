const validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateEditUserInput(data) {
  let errors = {};
  // convert empty fields to an empty string so we can use validator functions
  data.name = !isEmpty(data.name) ? data.name.trim() : "";
  data.role = !isEmpty(data.role) ? data.role.trim() : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  // name checks
  if (validator.isEmpty(data.name)) {
    errors.name = "Name field is required";
  }
  // role checks
  if (['regular','owner','admin'].indexOf(data.role) == -1) {
    errors.role = "Role needs to be 'regular', 'owner' or 'admin'";
  }
  // email checks
  if (validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  } else if (!validator.isEmail(data.email)) {
    errors.email = "Email is invalid";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
