const validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateCreateReviewReplyInput(data) {
  let errors = {};
  // convert empty fields to an empty string so we can use validator functions
  data.reply = !isEmpty(data.reply) ? data.reply.trim() : "";
  // reply checks
  if (validator.isEmpty(data.reply)) {
    errors.reply = "Reply field is required";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
