const validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateCreateRestaurantInput(data) {
  let errors = {};
  // convert empty fields to an empty string so we can use validator functions
  data.name = !isEmpty(data.name) ? data.name.trim() : "";
  // name checks
  if (validator.isEmpty(data.name)) {
    errors.name = "Name field is required";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
