const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const connectDB = require('./config/db');
const passport = require("passport");
const all = require("./routes/api/all");
const users = require("./routes/api/users");
const restaurants = require("./routes/api/restaurants");
const reviews = require("./routes/api/reviews");

const port = process.env.PORT || 4000;
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

connectDB();

// passport middleware
app.use(passport.initialize());
// passport config
require("./config/passport")(passport);

// runs first on all routes
app.use(all);

// routes
app.use("/api/users", users);
app.use("/api/restaurants", restaurants);
app.use("/api/reviews", reviews);

app.listen(port, () => console.log(`Server running on port ${port}`));
