module.exports = (func) => {
  return async (req, res, next) => {
    try {
      await func(req, res, next);
    }
    catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }
}
