const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const errorHandler = require("../../functions/errorHandler");
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
const validateEditUserInput = require("../../validation/editUser");

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", errorHandler(async (req, res) => {
  // form validation
  const { errors, isValid } = validateRegisterInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // try to find user by email
  const user = await User.findOne({ email: req.body.email });
  // check if user already exists
  if (user) return res.status(400).json({ email: "Email already exists" });
  // user does not already exist, create a new one
  const newUser = new User({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  });
  // hash password before saving it in the database
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(newUser.password, salt);
  newUser.password = hash;
  newUser.save().then(user => res.json(user));
}));

// @route POST api/users/login
// @desc Login user and return json web token
// @access Public
router.post("/login", errorHandler(async (req, res) => {
  // form validation
  const { errors, isValid } = validateLoginInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // try to find user by email
  const user = await User.findOne({ email: req.body.email });
  // check if user exists
  if (!user) return res.status(400).json({ email: "Email not found" });
  // user does exist, check password
  const isMatch = await bcrypt.compare(req.body.password, user.password);
  // if password does not match, return error
  if (!isMatch) return res.status(400).json({ password: "Password incorrect" });
  // create json web token payload
  const payload = {
    id: user.id,
    name: user.name,
    role: user.role,
  };
  // sign token
  const token = await jwt.sign(
    payload,
    keys.secretOrKey,
    { expiresIn: 31556926 }, // one year in seconds
  );
  // return object containing token
  return res.json({
    success: true,
    token: "Bearer " + token,
  });
}));

// @route GET api/users
// @desc Get all users
// @access Public
router.get("/", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // find and return all users
  User.find({}).then(users => res.json(users));
}));

// @route DELETE api/users/:id
// @desc Delete user
// @access Public
router.delete("/:id", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // delete user by id
  User.findOneAndDelete({ _id: req.params.id }).then(user => res.json(user));
}));

// @route PUT api/users/:id
// @desc Update user
// @access Public
router.put("/:id", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // form validation
  const { errors, isValid } = validateEditUserInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // find user by id
  const user = await User.findById(req.params.id);
  // update user
  user.name = req.body.name;
  user.role = req.body.role;
  user.email = req.body.email;
  // save to database
  user.save().then(user => res.json(user));
}));

module.exports = router;
