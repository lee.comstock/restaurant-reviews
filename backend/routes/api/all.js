const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const errorHandler = require("../../functions/errorHandler");
const User = require("../../models/User");
const Restaurant = require("../../models/Restaurant");
const Review = require("../../models/Review");

// runs first on all routes
module.exports = errorHandler(async (req, res, next) => {
  // don't check authorization for login and register user
  if (req.url.includes('/api/users/register') || req.url.includes('/api/users/login')) {
    return next();
  }
  // if no authorization token, send error message back
  if (!req.headers.authorization) {
    return res.status(403).json({ error: 'No credentials sent' });
  }
  // remove 'Bearer' from authorization token
  const token = req.headers.authorization.split(' ')[1];
  // check authorization token
  const decoded = jwt.verify(token, keys.secretOrKey);
  // find user by id
  const user = await User.findById(decoded.id);
  // check if user was found
  if (!user) return res.status(400).json({ error: 'No user found' });
  // store user data for next route handler
  res.locals.user = user;
  // move on to the next route handler
  return next();
});
