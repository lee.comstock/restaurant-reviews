const express = require("express");
const router = express.Router();
const errorHandler = require("../../functions/errorHandler");
const validateCreateReviewInput = require("../../validation/createReview");
const validateCreateReviewReplyInput = require('../../validation/createReviewReply');

// @route POST api/reviews
// @desc Create review
// @access Public
router.post("/", errorHandler(async (req, res) => {
  // check that user has the role 'regular'
  if (res.locals.user.role != 'regular') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // form validation
  const { errors, isValid } = validateCreateReviewInput(req.body.form);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // try to find a review posted by the user for this restaurant
  const review = await Review.findOne({
    poster: res.locals.user.id,
    restaurant: req.body.restaurant._id,
  });
  // check if review was found
  if (review) return res.status(400).json({
    error: 'This user has already created a review for this restaurant',
  });
  // create a new review and save it in the database
  const newReview = await new Review({
    comment: req.body.form.comment,
    date: req.body.form.date,
    rating: req.body.form.rating,
    poster: res.locals.user.id,
    restaurant: req.body.restaurant._id,
  }).save();
  // return new review
  return res.json(newReview);
}));

// @route POST api/reviews/:id/reply
// @desc Create review reply
// @access Public
router.post("/:id/reply", errorHandler(async (req, res) => {
  // form validation
  const { errors, isValid } = validateCreateReviewReplyInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // find review by id
  const review = await Review.findById(req.params.id);
  // check if review was found
  if (!review) return res.status(400).json({ error: 'No review found' });
  // find the restaurant that the review is for
  const restaurant = await Restaurant.findById(review.restaurant);
  // make sure that the restaurant is owned by the user making the request
  if (restaurant.owner != res.locals.user.id) {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // set review reply to the form data sent in
  review.reply = req.body.reply;
  // save changes to database
  review.save().then(review => res.json(review));
}));

// @route DELETE api/reviews/:id
// @desc Delete review
// @access Public
router.delete("/:id", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // delete review by id
  Review.findOneAndDelete({ _id: req.params.id }).then(review => res.json(review));
}));

// @route PUT api/reviews/:id
// @desc Update review
// @access Public
router.put("/:id", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // form validation
  const { errors, isValid } = validateCreateReviewInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // find review by id
  const review = await Review.findById(req.params.id);
  // update review
  review.comment = req.body.comment;
  review.date = req.body.date;
  review.rating = req.body.rating;
  if (req.body.reply) review.reply = req.body.reply;
  // save to database
  review.save().then(review => res.json(review));
}));

module.exports = router;
