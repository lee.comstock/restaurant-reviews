const express = require("express");
const router = express.Router();
const errorHandler = require("../../functions/errorHandler");
const validateCreateRestaurantInput = require("../../validation/createRestaurant");

// @route POST api/restaurants
// @desc Create restaurant
// @access Public
router.post("/", errorHandler(async (req, res) => {
  // check that user has the role 'owner'
  if (res.locals.user.role != 'owner') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // form validation
  const { errors, isValid } = validateCreateRestaurantInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // create a new restaurant and save it in the database
  const newRestaurant = await new Restaurant({
    name: req.body.name,
    owner: res.locals.user.id,
  }).save();
  // return new restaurant
  return res.json(newRestaurant);
}));

// @route GET api/restaurants
// @desc Get all restaurants
// @access Public
router.get("/", errorHandler(async (req, res) => {
  // find all restaurants
  const restaurants = await Restaurant.find({});
  // find all reviews
  const reviews = await Review.find({});
  // find all users
  const users = await User.find({});
  // create array of restaurants with an array of reviews for each restaurant
  let restaurantsAndReviews = restaurants.map(restaurant => ({
    ...restaurant._doc,
    reviews: reviews.filter(review => review.restaurant == restaurant.id).map(review => ({
      ...review._doc,
      posterData: users.find(user => user.id == review.poster),
    })).reverse(),
  }));
  // if user has the role 'owner', only return restaurants owned by the user
  if (res.locals.user.role == 'owner') {
    restaurantsAndReviews = restaurantsAndReviews.filter(restaurant => {
      return restaurant.owner == res.locals.user.id;
    });
  }
  // return restaurants and reviews
  return res.json(restaurantsAndReviews);
}));

// @route DELETE api/restaurants/:id
// @desc Delete restaurant
// @access Public
router.delete("/:id", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // delete restaurant by id
  Restaurant.findOneAndDelete({ _id: req.params.id }).then(restaurant => res.json(restaurant));
}));

// @route PUT api/restaurants/:id
// @desc Update restaurant
// @access Public
router.put("/:id", errorHandler(async (req, res) => {
  // check that user has the role 'admin'
  if (res.locals.user.role != 'admin') {
    return res.status(403).json({ error: 'Permission denied' });
  }
  // form validation
  const { errors, isValid } = validateCreateRestaurantInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // find restaurant by id
  const restaurant = await Restaurant.findById(req.params.id);
  // update restaurant
  restaurant.name = req.body.name;
  // save to database
  restaurant.save().then(restaurant => res.json(restaurant));
}));

module.exports = router;
