import axios from 'axios';

function setAuthToken(token) {
  token
  // if token, apply authorization token to every request if logged in
  ? axios.defaults.headers.common["Authorization"] = token
  // if no token, delete authorization header
  : delete axios.defaults.headers.common["Authorization"];
}

export default setAuthToken;
