function getAverageRestaurantRating(restaurant) {
  // return zero if restaurant has no reviews
  if (!restaurant.reviews.length) return 0;
  // add together all rating numbers
  let totalRatingNumber = 0;
  for (let review of restaurant.reviews) {
    totalRatingNumber += review.rating;
  }
  // return the average rating, total number divided by number of reviews, two decimals
  return parseFloat((totalRatingNumber / restaurant.reviews.length).toFixed(2));
}

export default getAverageRestaurantRating;
