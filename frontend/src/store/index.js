import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import setAuthToken from '../functions/setAuthToken';
import store from './index';
import { proxy } from '../config/proxy';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // currently logged in user
    user: {},
    // list of users retrieved from the backend
    userList: [],
    // list of restaurants retrieved from the backend
    restaurantList: [],
    // selected restaurant for detailed view
    selectedRestaurant: {},
  },
  mutations: {
    stayLoggedIn(state) {
      // check for token to keep user logged in
      if (localStorage.jwtToken) {
        // set auth token header auth
        const token = localStorage.jwtToken;
        setAuthToken(token);
        // decode token and get user info
        const decoded = jwt_decode(token);
        // set current user
        state.user = decoded;
        // check for expired token
        const currentTime = Date.now() / 1000; // to get in milliseconds
        if (decoded.exp < currentTime) {
          // logout user
          store.commit('logout');
        }
      }
    },
    logout(state) {
      // remove token from local storage
      localStorage.removeItem("jwtToken");
      // remove auth header for future requests
      setAuthToken(false);
      // set current user to empty object
      state.user = {};
      // set selected restaurant to empty object
      state.selectedRestaurant = {};
      // empty list of restaurants
      state.restaurantList = [];
      // empty list of users
      state.userList = [];
    },
    loginSuccessful(state, { response }) {
      // get token from response
      const { token } = response.data;
      // save token in local storage
      localStorage.setItem("jwtToken", token);
      // set token to auth header
      setAuthToken(token);
      // decode token to get user data
      const decoded = jwt_decode(token);
      // set current user
      state.user = decoded;
    },
    createReviewSuccessful(state, { review }) {
      // add review to selected restaurant in the frontend
      state.selectedRestaurant.reviews.push({
        ...review,
        posterData: {
          ...state.user,
        },
      });
    },
    createReviewReplySuccessful(state, { reply, review }) {
      // add review reply to the review in the frontend
      Vue.set(review, 'reply', reply);
    },
    getUsersSuccessful(state, { users }) {
      // set user list
      state.userList = users;
    },
    editUserSuccessful(state, { user, index }) {
      // replace old user with new user
      state.userList.splice(index, 1, user);
    },
    deleteUserSuccessful(state, { index }) {
      // remove user from user list
      state.userList.splice(index, 1);
    },
    deleteReviewSuccessful(state, { review }) {
      // remove review from selected restaurant
      state.selectedRestaurant.reviews = state.selectedRestaurant.reviews.filter(r => {
        return r._id !== review._id;
      });
    },
    deleteRestaurantSuccessful(state, { restaurant }) {
      // set selected restaurant to empty object
      state.selectedRestaurant = {};
      // remove restaurant from restaurant list
      state.restaurantList = state.restaurantList.filter(r => {
        return r._id !== restaurant._id;
      });
    },
    editReviewSuccessful(state, { review }) {
      // find the edited review in the array of reviews and update it in the frontend
      for (let i = 0; i < state.selectedRestaurant.reviews.length; i ++) {
        if (state.selectedRestaurant.reviews[i]._id == review._id) {
          state.selectedRestaurant.reviews.splice(i, 1, {
            ...review,
            posterData: {
              ...state.selectedRestaurant.reviews[i].posterData,
            },
          });
          break;
        }
      }
    },
    editRestaurantSuccessful(state, { restaurant }){
      // update restaurant in the frontend
      state.selectedRestaurant.name = restaurant.name;
    },
    getRestaurantsSuccessful(state, { restaurants }) {
      // set restaurant list
      state.restaurantList = restaurants;
    },
    addRestaurantToList(state, { restaurant }) {
      // add restaurant to list
      state.restaurantList.push({
        ...restaurant,
        reviews: [],
      });
    },
    setSelectedRestaurant(state, { restaurant }) {
      // set selected restaurant to empty object first to force dom rerender
      state.selectedRestaurant = {};
      // set selected restaurant after one tick
      setTimeout(() => {
        state.selectedRestaurant = restaurant;
      }, 0);
    },
  },
  actions: {

  },
  modules: {

  },
})
